<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('client.inc.php');
$section = 'home';
require(CLIENTINC_DIR.'header.inc.php');
?>
<div id="landing_page">
    <?php
    if($cfg && ($page = $cfg->getLandingPage()))
        echo $page->getBodyWithImages();
    else
        echo  '<h1>Bienvenido al Centro de Ayuda</h1>';
    ?>
    <div id="new_ticket">
        <h3>Abrir un Nuevo Ticket</h3>
        <br>
        <div>Por favor proporcione tanto detalle como sea posible para poder ayudarle mejor. Para actualizar un ticket enviado con anterioridad por favor utilice el formulario que aparece a la derecha.</div>
        <p>
            <a href="open.php" class="green button">Abrir un Nuevo Ticket</a>
        </p>
    </div>

    <div id="check_status">
        <h3>Ver Estado de un Ticket</h3>
        <br>
        <div>Contamos con los archivos y la historia de todas sus solicitudes de soporte, actuales y pasados, ​​completas con las respuestas.</div>
        <p>
            <a href="view.php" class="blue button">Revisar Status de Ticket</a>
        </p>
    </div>
</div>
<div class="clear"></div>
<?php
if($cfg && $cfg->isKnowledgebaseEnabled()){
    //FIXME: provide ability to feature or select random FAQs ??
?>
<p>Asegurese de visitar nuestras <a href="kb/index.php">Preguntas de Uso Frecuente (FAQs)</a>, antes de abrir un ticket.</p>
</div>
<?php
} ?>
<?php require(CLIENTINC_DIR.'footer.inc.php'); ?>
