<?php
/*********************************************************************
    ajax.content.php

    AJAX interface for content fetching...allowed methods.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

if(!defined('INCLUDE_DIR')) die('!');

class ContentAjaxAPI extends AjaxController {

    function log($id) {

        if($id && ($log=Log::lookup($id))) {
            $content=sprintf('<div
                    style="width:500px;">&nbsp;<strong>%s</strong><br><p
                    style="white-space:pre-line;">%s</p>
                    <hr><strong>Log Date:</strong> <em>%s</em> <strong>Dirección IP:</strong> <em>%s</em></div>',
                    $log->getTitle(),
                    Format::display(str_replace(',',', ',$log->getText())),
                    Format::db_daydatetime($log->getCreateDate()),
                    $log->getIP());
        }else {
            $content='<div style="width:295px;">&nbsp;<strong>Error:</strong>Log ID desconocido o no válido</div>';
        }

        return $content;
    }

    function ticket_variables() {

        $content='
<div style="width:680px;">
    <h2>Variables del Ticket</h2>
    Tenga en cuenta que las variables no básicas depende del contexto de uso. Visita osTicket Wiki de documentación hasta la fecha.
    <br/>
    <table width="100%" border="0" cellspacing=1 cellpadding=2>
        <tr><td width="55%" valign="top"><b>Variables Base</b></td><td><b>Otras variables</b></td></tr>
        <tr>
            <td width="55%" valign="top">
                <table width="100%" border="0" cellspacing=1 cellpadding=1>
                    <tr><td width="130">%{ticket.id}</td><td>ID del Ticket</td></tr>
                    <tr><td>%{ticket.number}</td><td>ID interno</td></tr>
                    <tr><td>%{ticket.email}</td><td>Email </td></tr>
                    <tr><td>%{ticket.name}</td><td>Nombre completo&mdash;
                        <em>see name expansion</em></td></tr>
                    <tr><td>%{ticket.subject}</td><td>Asunto</td></tr>
                    <tr><td>%{ticket.phone}</td><td>N° de Telefono | ext</td></tr>
                    <tr><td>%{ticket.status}</td><td>Estado</td></tr>
                    <tr><td>%{ticket.priority}</td><td>Prioridad</td></tr>
                    <tr><td>%{ticket.assigned}</td><td>Personal/Equipo asignado</td></tr>
                    <tr><td>%{ticket.create_date}</td><td>Fecha Creación</td></tr>
                    <tr><td>%{ticket.due_date}</td><td>Fecha Límite</td></tr>
                    <tr><td>%{ticket.close_date}</td><td>Fecha de Cierre</td></tr>
                    <tr><td>%{ticket.auth_token}</td><td>Símbolo de autenticación usado para auto-login</td></tr>
                    <tr><td>%{ticket.client_link}</td><td>Enlace a vista clientes del ticket</td></tr>
                    <tr><td>%{ticket.staff_link}</td><td>Enlace a vista del Personal del ticket</td></tr>
                    <tr><td colspan="2" style="padding:5px 0 5px 0;"><em>Variables expandibles (Ver Wiki)</em></td></tr>
                    <tr><td>%{ticket.<b>topic</b>}</td><td>Temas de Ayuda</td></tr>
                    <tr><td>%{ticket.<b>dept</b>}</td><td>Departmento</td></tr>
                    <tr><td>%{ticket.<b>staff</b>}</td><td>Personal Asignado</td></tr>
                    <tr><td>%{ticket.<b>team</b>}</td><td>Equipo Asignado</td></tr>
                </table>
            </td>
            <td valign="top">
                <table width="100%" border="0" cellspacing=1 cellpadding=1>
                    <tr><td width="100">%{message}</td><td>Mensaje entrante</td></tr>
                    <tr><td>%{response}</td><td>Mensaje saliente</td></tr>
                    <tr><td>%{comments}</td><td>Asignar/transferir Comentarios</td></tr>
                    <tr><td>%{note}</td><td>Internal note <em>(expandable)</em></td></tr>
                    <tr><td>%{assignee}</td><td>Personal/Equipo Asignado</td></tr>
                    <tr><td>%{assigner}</td><td>Personal asignando el ticket</td></tr>
                    <tr><td>%{url}</td><td>osTickets url </td></tr>
                    <tr><td>%{reset_link}</td>
                        <td>Reiniciar link usado para cambiar el password</td></tr>
                </table>
                <table width="100%" border="0" cellspacing=1 cellpadding=1>
                    <tr><td colspan="2"><b>Name Expansion</b></td></tr>
                    <tr><td>.first</td><td>First Name</td></tr>
                    <tr><td>.middle</td><td>Middle Name(s)</td></tr>
                    <tr><td>.last</td><td>Last Name</td></tr>
                    <tr><td>.full</td><td>First Last</td></tr>
                    <tr><td>.legal</td><td>First M. Last</td></tr>
                    <tr><td>.short</td><td>First L.</td></tr>
                    <tr><td>.formal</td><td>Mr. Last</td></tr>
                    <tr><td>.shortformal</td><td>F. Last</td></tr>
                    <tr><td>.lastfirst</td><td>Last, First</td></tr>
                </table>
            </td>
        </tr>
    </table>
</div>';

        return $content;
    }

    function getSignature($type, $id=null) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Requerido');

        switch ($type) {
        case 'none':
            break;
        case 'mine':
            echo Format::viewableImages($thisstaff->getSignature());
            break;
        case 'dept':
            if (!($dept = Dept::lookup($id)))
                Http::response(404, 'No existe ese departamento');
            echo Format::viewableImages($dept->getSignature());
            break;
        default:
            Http::response(400, 'No hay el tipo de Firma');
            break;
        }
    }

    function manageContent($id, $lang=false) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Requerido');

        $content = Page::lookup($id, $lang);
        include STAFFINC_DIR . 'templates/content-manage.tmpl.php';
    }

    function manageNamedContent($type, $lang=false) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Requerido');

        $content = Page::lookup(Page::getIdByType($type, $lang));
        include STAFFINC_DIR . 'templates/content-manage.tmpl.php';
    }

    function updateContent($id) {
        global $thisstaff;

        if (!$thisstaff)
            Http::response(403, 'Login Requerido');
        elseif (!$_POST['name'] || !$_POST['body'])
            Http::response(422, 'Captura el nombre y cuerpo');
        elseif (!($content = Page::lookup($id)))
            Http::response(404, 'No hay contenido');

        $vars = array_merge($content->getHashtable(), $_POST);
        $errors = array();
        if (!$content->save($id, $vars, $errors)) {
            if ($errors['err'])
                Http::response(422, $errors['err']);
            else
                Http::response(500, 'No se pudo actualizar : '.print_r($errors, true));
        }
    }
}
?>
