<?php
if(!defined('OSTCLIENTINC')) die('Acceso Denegado');

$userid=Format::input($_POST['userid']);
?>
<h1>Olvide mi Contraseña</h1>
<p>
Captura tu clave de usuario o tu dirección de correo en el formulario de 
abajo y presiona <strong>Login</strong> para accesar a la cuenta y 
recuperar tu Contraseña.

<form action="pwreset.php" method="post" id="clientLogin">
    <div style="width:50%;display:inline-block">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="reset"/>
    <input type="hidden" name="token" value="<?php echo $_REQUEST['token']; ?>"/>
    <strong><?php echo Format::htmlchars($banner); ?></strong>
    <br>
    <div>
        <label for="username">Clave de usuario:</label>
        <input id="username" type="text" name="userid" size="30" value="<?php echo $userid; ?>">
    </div>
    <p>
        <input class="btn" type="submit" value="Login">
    </p>
    </div>
</form>
