<?php if ($content) {
    list($title, $body) = $ost->replaceTemplateVariables(
        array($content->getName(), $content->getBody())); ?>
<h1><?php echo Format::display($title); ?></h1>
<p><?php
echo Format::display($body); ?>
</p>
<?php } else { ?>
<h1>Registro de Cuenta</h1>
<p>
<strong>Gracias por registrar la Cuenta.</strong>
</p>
<p>
Hemos enviado un correo ala dirección que capturaste. Favor de seguir el
link de confirmacion que se envio a tu Email para tener acceso a tus tickets.
</p>
<?php } ?>
