<?php
if(!defined('OSTCLIENTINC')) die('Access Denied');

$email=Format::input($_POST['lemail']?$_POST['lemail']:$_GET['e']);
$ticketid=Format::input($_POST['lticket']?$_POST['lticket']:$_GET['t']);
?>
<h1>Checar estatus de Ticket</h1>
<p>Favor de Proveer la dirección de Email y el número de ticket, Y un link 
de acesso sera enviado a tu correo.</p>
<form action="login.php" method="post" id="clientLogin">
    <?php csrf_token(); ?>
<div style="display:table-row">
    <div style="width:40%;display:table-cell;box-shadow: 12px 0 15px -15px rgba(0,0,0,0.4);padding-right: 2em;">
    <strong><?php echo Format::htmlchars($errors['login']); ?></strong>
    <br>
    <div>
        <label for="email">Dirección de E-Mail:
        <input id="email" placeholder="ej. john.doe@nearbpo.com" type="text"
            name="lemail" size="30" value="<?php echo $email; ?>"></label>
    </div>
    <div>
        <label for="ticketno">Número de Ticket:</label><br/>
        <input id="ticketno" type="text" name="lticket" placeholder="ej. 051243"
            size="30" value="<?php echo $ticketid; ?>"></td>
    </div>
    <p>
        <input class="btn" type="submit" value="Link de Acceso a Email">
    </p>
    </div>
    <div style="display:table-cell;padding-left: 2em;padding-right:90px;">
<?php if ($cfg && $cfg->getClientRegistrationMode() !== 'disabled') { ?>
        Tienes una cuenta con nosotros?
        <a href="login.php">Registrar</a> <?php
    if ($cfg->isClientRegistrationEnabled()) { ?>
        o <a href="account.php?do=create">registrate por una cuenta</a> <?php
    } ?> para acceder a todos tus tickets.
<?php
} ?>
    </div>
</div>
</form>
<br>
<p>
Si esta es tu primera vez contactandonos o has perdido tu número de ticket, Favor de <a href="open.php"> abrir un nuevo ticket</a>.
</p>
