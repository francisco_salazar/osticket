<?php

if(!defined('OSTCLIENTINC') || !$thisclient || !$ticket || !$ticket->checkUserAccess($thisclient)) die('Acceso Denegado!');

?>

<h1>
    Editando el Ticket #<?php echo $ticket->getNumber(); ?>
</h1>

<form action="tickets.php" method="post">
    <?php echo csrf_token(); ?>
    <input type="hidden" name="a" value="editar"/>
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>"/>
<table width="800">
    <tbody id="dynamic-form">
    <?php if ($forms)
        foreach ($forms as $form) {
            $form->render(false);
    } ?>
    </tbody>
</table>
<hr>
<p style="text-align: center;">
    <input type="submit" value="Actualizar"/>
    <input type="reset" value="Resetear"/>
    <input type="button" value="Cancelar" onclick="javascript:
        window.location.href='index.php';"/>
</p>
</form>
