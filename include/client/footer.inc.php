        </div>
    </div>
    <div id="footer">
        <p>Copyright &copy; <?php echo date('Y'); ?> <?php echo (string) $ost->company ?: 'osTicket.com'; ?> - All rights reserved.</p>
        <a id="poweredBy" href="http://osticket.com" target="_blank">Helpdesk software - powered by osTicket</a>
    </div>
<div id="overlay"></div>
<div id="loading">
    <h4>Favor de esperar!</h4>
    <p>Favor de esperar ... Tomara un segundo!</p>
</div>
</body>
</html>
