<?php
if(!defined('OSTCLIENTINC')) die('Acceso Denegado');

$userid=Format::input($_POST['userid']);
?>
<h1>Olvide mi Password</h1>
<p>
Captura tu clave de Usuario o dirección Email en el formulario de abajo 
y presiona el boton <strong>Enviar Email</strong> para que se te envia 
el reinicio de la Contraseña a tu Email.

<form action="pwreset.php" method="post" id="clientLogin">
    <div style="width:50%;display:inline-block">
    <?php csrf_token(); ?>
    <input type="hidden" name="do" value="sendmail"/>
    <strong><?php echo Format::htmlchars($banner); ?></strong>
    <br>
    <div>
        <label for="username">Clave de Usuario:</label>
        <input id="username" type="text" name="userid" size="30" value="<?php echo $userid; ?>">
    </div>
    <p>
        <input class="btn" type="submit" value="Enviar Email">
    </p>
    </div>
</form>
