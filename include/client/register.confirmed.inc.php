<?php if ($content) {
    list($title, $body) = $ost->replaceTemplateVariables(
        array($content->getName(), $content->getBody())); ?>
<h1><?php echo Format::display($title); ?></h1>
<p><?php
echo Format::display($body); ?>
</p>
<?php } else { ?>
<h1>Registro de Cuenta</h1>
<p>
<strong>Gracias por registrar tu cuenta.</strong>
</p>
<p>
Has confirmado tu dirección de Email exitosamente y activado tu cuenta. 
Puedes proceder a checar los tickets que hayas dado de alta.
</p>
<p><em></em></p>
<?php } ?>
